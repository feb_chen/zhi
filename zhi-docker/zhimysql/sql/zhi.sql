SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attachment
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '附件主键',
  `resourceId` int NULL DEFAULT NULL COMMENT '附件所属资源Id',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '上传时间',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `text` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '解析文本',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '解析状态',
  `overTime` timestamp NULL DEFAULT NULL COMMENT '解析结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of attachment
-- ----------------------------

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `resource_id` int NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 342 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for cron
-- ----------------------------
DROP TABLE IF EXISTS `cron`;
CREATE TABLE `cron`  (
  `cron_id` int NOT NULL AUTO_INCREMENT,
  `cron` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tikaCron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`cron_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cron
-- ----------------------------
INSERT INTO `cron` VALUES (1, '0 0/3 * * * ? ', '0 0/5 * * * ? ');

-- ----------------------------
-- Table structure for group_info
-- ----------------------------
DROP TABLE IF EXISTS `group_info`;
CREATE TABLE `group_info`  (
  `group_id` int NOT NULL AUTO_INCREMENT COMMENT '用户组id',
  `crop_id` int NULL DEFAULT 0 COMMENT 'cropid',
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户组名字',
  `group_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '用户组描述',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '建立时间',
  `edit_time` timestamp NULL DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of group_info
-- ----------------------------

-- ----------------------------
-- Table structure for label_info
-- ----------------------------
DROP TABLE IF EXISTS `label_info`;
CREATE TABLE `label_info`  (
  `label_id` int NOT NULL AUTO_INCREMENT COMMENT '标签id',
  `crop_id` int NULL DEFAULT 0,
  `label_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `label_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `uplevel_id` int NULL DEFAULT NULL COMMENT '默认为0，即为一级标签，如果不为零则为对应label_id的下级标签',
  `create_time` timestamp NULL DEFAULT NULL,
  `edit_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`label_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 202 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of label_info
-- ----------------------------
INSERT INTO `label_info` VALUES (7, NULL, '项目', '1', 0, '2020-09-23 20:56:32', '2021-01-05 01:05:34');
INSERT INTO `label_info` VALUES (8, NULL, '类型', '2', 0, '2020-09-23 21:02:16', '2021-01-05 01:05:28');
INSERT INTO `label_info` VALUES (23, NULL, '主题', '3', 0, '2020-09-28 11:37:32', '2021-01-05 01:05:19');
INSERT INTO `label_info` VALUES (201, NULL, '书籍', '123', 8, '2021-08-06 10:59:06', '2021-08-06 10:59:06');

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '资源id',
  `crop_id` int NULL DEFAULT 0 COMMENT 'cropid',
  `user_id` int UNSIGNED NULL DEFAULT 0 COMMENT '创作者id',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '资源新建时间',
  `edit_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '标题',
  `text` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '正文文本',
  `attachment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '附件的url地址（一个或者多个），采用json格式保存',
  `superior` int NULL DEFAULT 0 COMMENT '资源上级（if =0，则为原帖；if=某资源id，则为评论）',
  `recognition` int NULL DEFAULT 0 COMMENT '点赞数量',
  `opposition` int NULL DEFAULT 0 COMMENT '反对数量',
  `pageview` int NULL DEFAULT 0 COMMENT '浏览数量',
  `collection` int NULL DEFAULT 0 COMMENT '收藏数量',
  `group_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '资源所属用户组id（一个或者多个或者公开），采用json格式保存',
  `label_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '资源的标签id（一个或者多个），采用json格式保存',
  `permissionId` int NULL DEFAULT 0 COMMENT '资源是否为公开资源（0为非公开，1为公开）',
  `contents` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '书籍目录',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17501 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of resource
-- ----------------------------

-- ----------------------------
-- Table structure for signup_info
-- ----------------------------
DROP TABLE IF EXISTS `signup_info`;
CREATE TABLE `signup_info`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `telnumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '申请理由',
  `status` int NULL DEFAULT NULL COMMENT '申请状态（0为待审核，1为已通过，2为已拒绝，3为其他）',
  `signup_time` timestamp NULL DEFAULT NULL COMMENT '申请时间',
  `process_time` timestamp NULL DEFAULT NULL COMMENT '处理时间',
  `crop_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of signup_info
-- ----------------------------

-- ----------------------------
-- Table structure for spec_resource
-- ----------------------------
DROP TABLE IF EXISTS `spec_resource`;
CREATE TABLE `spec_resource`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `resource_id` int NULL DEFAULT NULL COMMENT '资源id',
  `spec_order` int NULL DEFAULT NULL COMMENT '资源排序',
  `type` int NULL DEFAULT NULL COMMENT '1表示推荐，2表示公告',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '资源调整时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of spec_resource
-- ----------------------------

-- ----------------------------
-- Table structure for system_manage
-- ----------------------------
DROP TABLE IF EXISTS `system_manage`;
CREATE TABLE `system_manage`  (
  `variable` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `set_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of system_manage
-- ----------------------------
INSERT INTO `system_manage` VALUES ('appKey_public', '88888888', '2022-03-15 19:52:30');
INSERT INTO `system_manage` VALUES ('default_password', '666666', '2022-03-25 15:00:09');
INSERT INTO `system_manage` VALUES ('book_label', '201', '2021-08-18 20:31:22');
INSERT INTO `system_manage` VALUES ('es_addr', '0.0.0.0', '2021-09-05 15:53:12');
INSERT INTO `system_manage` VALUES ('es_period', 'unknown', '2021-09-05 15:54:57');
INSERT INTO `system_manage` VALUES ('kms_version', '2.0', '2021-09-05 15:53:20');
INSERT INTO `system_manage` VALUES ('mysql_addr', '0.0.0.0', '2021-09-05 15:53:22');
INSERT INTO `system_manage` VALUES ('nfs_addr', '0.0.0.0', '2021-09-05 15:53:27');
INSERT INTO `system_manage` VALUES ('nfs_increment', 'unknown', '2021-08-12 16:32:07');
INSERT INTO `system_manage` VALUES ('nfs_total', 'unknown', '2021-08-12 16:32:14');
INSERT INTO `system_manage` VALUES ('tika_addr', '0.0.0.0', '2021-08-11 17:24:49');
INSERT INTO `system_manage` VALUES ('tika_period', 'unknown', '2021-09-05 15:55:00');
INSERT INTO `system_manage` VALUES ('tika_retry', 'unknown', '2021-09-05 15:55:04');


-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `templateName` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `templateCode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `createTime` timestamp NULL DEFAULT NULL,
  `setTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of template
-- ----------------------------
INSERT INTO `template` VALUES (1, '周报', '<h3>1. 本周工作总结</h3><table style=\"border-collapse: collapse; border: none; width: 100%;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr style=\"height: 19px;\"><td style=\"width: 30%; border: 1pt solid;\" colspan=\"2\" valign=\"top\">时段</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">工作内容</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周一</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周二</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周三</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周四</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周五</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周六</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" rowspan=\"3\" valign=\"top\">周日</td><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">上午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">下午</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr><tr style=\"height: 19px;\"><td style=\"width: 15%; border: 1pt solid;\" valign=\"top\">晚间</td><td style=\"width: 70%; border: 1pt solid;\" valign=\"top\">XXXXXXXXXXX<br />XXXXXXXXXXX</td></tr></tbody></table><br /><h3>2. 本周未解决的问题</h3><ol><li>XXXXXXXXXXXXXXXXXXXXXX</li><li>XXXXXXXXXXXXXXXXXXXXXX</li></ol><h3>3. 下周工作计划</h3><ol><li>XXXXXXXXXXXXXXXXXXXXXX</li><li>XXXXXXXXXXXXXXXXXXXXXX</li></ol><h3>4. 对项目的建议</h3><ol><li>XXXXXXXXXXXXXXXXXXXXXX</li><li>XXXXXXXXXXXXXXXXXXXXXX</li></ol>', '2021-04-27 01:10:32', '2021-04-27 01:11:29');
INSERT INTO `template` VALUES (2, '项目会议纪要', '<h2>一、时间</h2><p>YYYY.MM.DD HH-MM</p><h2>二、地点</h2><p>腾讯会议</p><h2>三、人员</h2><ul><li>团队：XXX、XXX、XXX、XXX、XXX</span></li><li>团队：XXX、XXX、XXX、XXX、XXX</span></li></ul><h2>四、主要内容</h2><p>本次会议主要针对xxxxxxxxxx等事宜开展交流与研讨。</p><h3>内容1</h3><p>对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍对会议内容1的介绍。</p><h3>内容2</h3><p>对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍对会议内容2的介绍。</p><h2>五、下一步工作</h2><p>本次会议后将主要针对如下事宜继续开展工作。</p><ol><li>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</li><li>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</li><li>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</li></ol>', '2021-04-30 17:56:58', '2021-04-30 18:11:00');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `crop_id` int NULL DEFAULT 0,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账户（手机号）',
  `user_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账户密码',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `account_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账户名（昵称）',
  `role_id` int NULL DEFAULT NULL COMMENT '用户类型：0普通，1超级管理员，2用户管理员，3内容管理员，4系统管理员',
  `app_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'openApi所使用的appKey',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 142 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 0, '11111111111', 'f379eaf3c831b04de153469d1bec345e', '超级管理员', 'ziyouzhiyi@bupt.edu.cn', 'zyzy', 1, '');
INSERT INTO `user` VALUES (39, 0, '12222222222', 'f379eaf3c831b04de153469d1bec345e', '用户管理员', 'yhgl@bupt.edu.cn', 'yhgl', 2, '');
INSERT INTO `user` VALUES (40, 0, '13333333333', 'f379eaf3c831b04de153469d1bec345e', '内容管理员', 'nrgl@bupt.edu.cn', 'nrgl', 3, '');
INSERT INTO `user` VALUES (41, 0, '14444444444', 'f379eaf3c831b04de153469d1bec345e', '系统管理员', 'xtgl@bupt.edu.cn', 'xtgl', 4, '');
INSERT INTO `user` VALUES (142, 0, '15555555555', 'f379eaf3c831b04de153469d1bec345e', '普通用户', 'ptyh@bupt.edu.cn', 'ptyh', 0, '');

-- ----------------------------
-- Table structure for user_group
-- ----------------------------
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `group_id` int NULL DEFAULT NULL COMMENT '用户组id',
  `permission` int NULL DEFAULT NULL COMMENT '组内角色，1为负责人，2为管理员，0为成员',
  `crop_id` int NULL DEFAULT NULL COMMENT 'crop_id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 292 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_group
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;

SET global time_zone = '+8:00';