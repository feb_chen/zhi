package com.free4inno.knowledgems.utils;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;


public class NarrowImg {
    /**
     * @param sourceImage 要缩放的源文件路径
     * @param targetImage 缩放后目标文件前缀
     * @param type 缩放后目标文件后缀
     */

    public boolean CreateThumbnail(String sourceImage,String targetImage,String type) throws Exception {
        double Ratio_width = 0.5; // 宽度的缩放比例
        double Ratio_height = 0.5; // 高度的缩放比例
        File F = new File(sourceImage);
        if (!F.isFile())
            throw new Exception(F+ " is not image file error in CreateThumbnail!");
        BufferedImage Bi = ImageIO.read(F);
        // 假设图片宽高最大为120 120
        Image Itemp = Bi.getScaledInstance(120, 120, BufferedImage.SCALE_SMOOTH);
//        Ratio_width = 120.0 / Bi.getHeight();
//        Ratio_height = 120.0 / Bi.getWidth();
        AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(Ratio_width, Ratio_height), null);
        Itemp = op.filter(Bi, null);
        F.delete();
        File targetFile = new File(targetImage+"."+type);
        try {
            ImageIO.write((BufferedImage) Itemp, type, targetFile);
        } catch (Exception ex) {
            throw new Exception(" ImageIo.write error in CreatThum.: "+ ex.getMessage());
        }
        return true;
    }
}