package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.ResourceES;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * ResourceESDao.
 */
@Repository
public interface ResourceESDao extends ElasticsearchRepository<ResourceES, Integer> {
    Page<ResourceES> findAll(Pageable pageable);

    Page<ResourceES> findResourceESByPermissionId(String permissionId, Pageable pageable);

    ResourceES findResourceESById(Integer id);

}
