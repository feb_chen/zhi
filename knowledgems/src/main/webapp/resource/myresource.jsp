<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2021/1/3
  Time: 18:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>我的-搜索-知了[团队知识管理应用]</title>
</head>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <div class="re-box-6 bg-grey-6 mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated">
                        <div id="search-data" class="resource-post">
                            <div class="resource-post-box pt-30 pb-25">
                                <div class="front-loading">
                                    <img src="../assets/images/loading.gif"/>
                                </div>
                                <div class="panel-body text-center">正在加载请稍候</div>
                            </div>
                        </div>
                        <div id="page-data" class="text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script type="text/javascript">
    var userId = ${userId};
    var userName = '${userName}';
    //加载动画的html
    var loadinghtml = "<div class=\"resource-post-box pt-30 pb-25\">" +
        "<div class=\"front-loading\">" +
        "<img src=\"../assets/images/loading.gif\"/>" +
        "</div>" +
        "<div class=\"panel-body text-center\">正在加载请稍候</div>" +
        "</div>"

    //异步获取该用户的资源列表
    function getPage(page) {
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../myresource/queryMyResourceData", {
            userId: userId,
            userName: userName,
            page: page - 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("resourceNum").value);
            $("#search-data").html(data);
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getPage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    $(document).ready(function () {
        getPage(1);
    })
</script>
<!-- END: Scripts -->
</html>
