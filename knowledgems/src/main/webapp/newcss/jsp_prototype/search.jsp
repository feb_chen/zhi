<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2021/7/18
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="../css/mycss.css">
</head>
<body>
<div class="dx-main">
    <c:import url="_navbar.jsp?menu=search"/>
    <div class="dx-box-6 bg-grey-6">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-xl-12">
                    <div class="dx-box-decorated mt-85">
                        <div class="dx-box-content">
                            <div class="dx-form-group">
                                <span class="mnt-7 font-title-2"><strong>&nbsp;搜&nbsp;索&nbsp;</strong></span>
                                <input type="text" class="form-control" id="query" name="queryStr"
                                       placeholder="在这里发现你想要的知识" value="${searchQueryStr}">
                            </div>
                            <br/>
                            <div class="dx-form-group">
                                <span class="mnt-7 font-title-2"><strong>&nbsp;群&nbsp;组&nbsp;</strong></span>
                                <div style="line-height:45px;">
                                        <button id="group${groupList.id}" class="dx-btn"
                                                style="background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;padding: 11px .75rem;"
                                                onclick="groupButton(${groupList.id})">群组名</button>
                                </div>
                            </div>
                            <br/>
                            <div class="dx-form-group">
                                <span class="mnt-7 font-title-2"><strong>&nbsp;分&nbsp;类&nbsp;</strong></span>
                                <div id="labels">
                                    <div class="col-md-12 row">
                                        <div>
                                            <a class="dx-btn dx-btn-text">测试</a>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="float-left mb-9">
                                                <button class="dx-btn dx-btn-label">测试</button>
                                            </div>
                                        </div>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="dx-form-group">
                                <button class="dx-btn dx-btn-center" type="submit" id="adsearch" onclick="adsearch()">搜 索</button>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
</html>
