<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/10/14
  Time: 20:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>注册-知了[团队知识管理应用]</title>
    <style>
        body{background-color: #1b1b1b}
    </style>
</head>
<body>
<div class="login-box" id="signup">
    <div class="login-box text-center" style="padding-top: 80px;">
        <h1 class="text-white mb-30">申请账号</h1>
        <form action="#" class="re-form">
            <div class="re-form-group-md">
                <input type="text" class="form-control" placeholder="姓名" id="name">
            </div>
            <div class="re-form-group-md">
                <input type="text" class="form-control" placeholder="手机号" id="telnumber">
            </div>
            <div class="re-form-group-md">
                <input type="email" class="form-control" placeholder="邮箱" id="mail">
            </div>
            <div class="re-form-group-md">
                <textarea rows="3" class="form-control" placeholder="申请理由" id="reason"></textarea>
            </div>
            <div class="re-form-group-md">
                <button id="submit" type="button" class="re-btn re-btn-block re-btn-lg" onclick="signup()">提交
                </button>
            </div>
        </form>
    </div>
</div>
<c:import url="/template/_include_js.jsp"/>
<script>
    function signup() {
        var name = $("#name").val();
        var mail = $("#mail").val();
        var telnumber = $("#telnumber").val();
        var reason = $("#reason").val();
        if ((name == null) || (name.length == 0) || (mail == null) || (mail.length == 0) ||
            (telnumber == null) || (telnumber.length == 0) || (reason == null) || (reason.length == 0)) {
            $.fillTipBox({type: 'warning', icon: 'glyphicon-alert', content: '请填写全部内容！'});
        } else if (isMobile(telnumber) !== 1) {
            $.fillTipBox({type: 'warning', icon: 'glyphicon-alert', content: '请填写正确格式的手机号码！'});
        } else if (isMail(mail) !== 1) {
            $.fillTipBox({type: 'warning', icon: 'glyphicon-alert', content: '请填写正确格式的邮箱！'});
        } else {
            $.ajax({
                type: "post",
                url: "/signup",
                data: {
                    name: name,
                    mail: mail,
                    telnumber: telnumber,
                    reason: reason
                },
                beforeSend: function () {
                    $("#submit").attr({disabled: "disabled"});
                },
                success: function (data) {
                    if (data == "success") {
                        $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '提交成功，请联系管理员审核通过！'});
                        window.setTimeout(function () {
                            window.location.href = "/";
                        }, 1500)
                    } else if (data == "false") {
                        $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '提交失败，手机号/邮箱已存在！'});
                        $("#submit").removeAttr("disabled");
                    }
                },
                error: function () {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '请求异常！'});
                }
            })
        }
    }

    //检查手机和邮箱格式
    function isMobile(phone) {
        let isPhone = 1;
        var phone = phone;
        if (!(phone.length == 11)) {
            isPhone = 0;
            return isPhone;
        }
        if (!(phone.substring(0, 1) == 1)) {
            isPhone = 0;
            return isPhone;
        }
        for (i = 0; i < phone.length; i++) {
            num = phone.substring(i, i + 1);
            if (!(num == 0 || num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9)) {
                isPhone = 0;
                return isPhone;
            }
        }
        return isPhone;
    }

    function isMail(mail) {
        let isMail = 0;
        var mail = mail;
        const mailLast = mail.substring(mail.length - 1, mail.length);
        for (i = 0; i < mail.length; i++) {
            var word = mail.substring(i, i + 1);
            if (!((word >= 'a' && word <= 'z') || (word >= 'A' && word <= 'Z') || (word >= '0' && word <= '9') || (word == "@") || (word == "_") || (word == "-") || (word == "."))) {
                return isMail;
            }
        }
        if (mailLast == "@") {
            return isMail;
        }
        if (mail.substring(0, 1) == "@") {
            return isMail;
        }
        for (i = 0; i < mail.length; i++) {
            num = mail.substring(i, i + 1);
            if (num == "@") {
                isMail = isMail + 1;
            }
        }
        return isMail;
    }
</script>
</body>
</html>
