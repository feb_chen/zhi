<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2020/9/14
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="/template/_header.jsp"/>
    <title>${groupInfo.groupName}-群组-知了[团队知识管理应用]</title>
</head>
<body>
<div class="re-main">
    <c:import url="/template/_navbar.jsp"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated">
                        <div class="resource-post">
                            <div class="resource-post-box">
                                <ul class="re-breadcrumbs pb-25" id="breadCrumb">
                                    <a href="javascript:void(0)" onclick="javascript :history.back(-1);">群组</a>&nbsp;|&nbsp;${groupInfo.groupName}
                                </ul>
                                <c:choose>
                                    <c:when test="${userPermission == 1 || roleID == 1}">
                                    <div class="resource-comment-text">
                                        <h1 class="h4 mnt-5 mb-9"><span contenteditable="true" id="groupName" onblur="submit(${groupId})">${groupInfo.groupName}</span></h1>
                                    </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="resource-comment-text">
                                            <h1 class="h4 mnt-5 mb-9">${groupInfo.groupName}</h1>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="re-separator"></div>
                            <div class="re-separator mnt-1"></div>

                            <div class="resource-post-box">
                                <h6 class="h6 px-25" id="members">成员
                                    <c:if test="${userPermission == 1 || userPermission ==2 || roleID == 1 || roleID ==2}">
                                        <span id="addMemberButton" style="float:right;color: darkgray;font-weight: normal">
                                            <a href="javascript:void(0);" onclick="showAddDialog1()">新增成员</a>
                                        </span>
                                    </c:if>
                                </h6>
                                <div id="member" class="resource-post" style="line-height:40px"></div>
                            </div>
                            <div class="re-separator"></div>
                            <div class="re-separator mnt-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="re-box-6 pb-25 re-grey" id="groupResource">
            <div class="container">
                <div class="row vertical-gap md-gap">
                    <div class="col-lg-12 pb-25">
                        <div class="re-box re-box-decorated">
                            <div id="search-data" class="resource-post">
                                <div class="resource-post-box pt-30 pb-25">
                                    <div class="front-loading">
                                        <img src="../assets/images/loading.gif"/>
                                    </div>
                                    <div class="panel-body text-center">正在加载请稍候</div>
                                </div>
                            </div>
                            <div id="page-data" class="text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <c:import url="../template/_footer.jsp"/>
</div>

<div class="modal fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel" data>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">选择成员</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>成员姓名</label>
                    <select class="selectpicker form-control" id="addName"data-live-search="true"></select>
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="addMember(${groupId})"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
</body>
<c:import url="../template/_include_js.jsp"/>

<script>
    var groupIds = ${groupId};
    var loadinghtml = "<div class=\"resource-post-box pb-25\">" +
        "<div class=\"front-loading\">" +
        "<img src=\"../assets/images/loading.gif\"/>" +
        "</div>" +
        "<div class=\"panel-body text-center\">正在加载请稍候</div>" +
        "</div>"

    $(document).ready(function () {
        getMember(${groupId});
    })

    document.addEventListener("keydown",function(e){
        if(e.keyCode == 13){
            document.getElementById("groupName").blur();
        }
    });

    function getMember(groupId) {
        $.ajax({
            type: "get",
            url: "/getMember",
            data: {groupId: groupId},
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                $("#member").html(data);
            }
        });
    }

    //异步获取搜索结果列表
    function getPage(page) {
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../search/searchdata", {
            queryStr: "",
            page: page - 1,
            groupIds: groupIds,
            labelIds: "",
            searchTimes: 1
        }, function (data) {
            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("appNum").value);
            $("#search-data").html(data);
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getPage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    function setLeader(groupId, userId){
        $.ajax({
            url:"/setLeader",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                userId:userId
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '更改群主成功'});
                    getMember(${groupId});
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '设置管理员失败'});
                }
            },
        });
    }

    function setAdmin(groupId, userId){
        $.ajax({
            url:"/setAdmin",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                userId:userId
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '设置管理员成功'});
                    getMember(${groupId});
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '设置管理员失败'});
                }
            },
        });
    }

    function cancelAdmin(groupId, userId){
        $.ajax({
            url:"/cancelAdmin",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                userId:userId
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '取消管理员成功'});
                    getMember(${groupId});
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '取消管理员失败'});
                }
            },
        });
    }

    function showAddDialog1(){
        $('#addMember').modal();
    }


    function addMember(groupId){
        var userAccountGet = $("#addName").val();
        $.ajax({
            url:"/addMember",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                userAccount:userAccountGet
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '添加成员成功'});
                    getMember(${groupId});
                    createSelectPicker();
                    $('#addName').selectpicker('val', '');


                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '删除成员失败'});
                }
            },
        });
    }

    function deleteMember(groupId, userId){
        $.ajax({
            url:"/deleteMember",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                userId:userId
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '删除成员成功'});
                    getMember(${groupId});
                    createSelectPicker();
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '删除成员失败'});
                }
            },
        });
    }

    function submit(groupId){
        var newName = document.getElementById('groupName').innerText;
        $.ajax({
            url:"/newEditGroup",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
                newName:newName,
            },
            success:function(data) {
                if(data=="ok") {
                    // $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '删除成员成功'});
                    <%--setInterval(function () {--%>
                    <%--    window.location = "/groupinfo?groupid=${groupId}";--%>
                    <%--}, 1000);--%>
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '删除成员失败'});
                }
            },
        });
    }

    /*
* 最初加载页面的时候，新增成员功能进行的搜索
* */
    $(document).ready(function () {
        //loading
        getPage(1);
        createSelectPicker();
    })

    //拼接增加成员的下拉框
    function createSelectPicker(){
        $.ajax({
            type: 'post',
            url:"/getNames",
            data:{groupId: ${groupId}},
            dataType: "json",
            success: function (data) {
                //获取数据成功时进行循环拼接下拉框;
                //首先清空下拉框，防止重复添加
                obj = document.getElementById("addName");
                for(i=obj.options.length-1 ; i>= 0 ; i--)
                    obj.options[i] = null;
                var tempIdStr = '<option  value="" selected：disable style="display: none">点击选择用户</option>';
                $("#addName").append(tempIdStr);
                for (var i = 0; i < data.length; i++){
                    var add_name = '<option value="' + data[i].split(" ")[2] + '">'+ data[i] + '</option>';
                    $('#addName').append(add_name);
                }
                //这一步很重要   更新
                $('#addName').selectpicker('refresh');
            }
        })
    }
</script>
<!-- END: Scripts -->
</html>
